#pragma once
#include <string>
#include <memory>
#include <list>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

namespace snippet {
	namespace filesystem {
		using namespace std;
		class path {
			string _path;
		public:
			path(const string&& pathname = {}) : _path(pathname) { ; }
			path(const path& pathname) { _path = pathname._path; }
			path& operator = (const path& pathname) { _path = pathname._path; return *this; }
			path& operator = (const string&& pathname) { _path = pathname; return *this; }
			path& operator = (const string& pathname) { _path = pathname; return *this; }
			path& operator / (const string&& pathname) { if (_path.empty() || _path.back() != '/') _path += '/';  _path += pathname; }
			path& operator / (const string& pathname) { if (_path.empty() || _path.back() != '/') _path += '/'; _path += pathname; }

			operator bool() { return _path.empty(); }
			bool empty() { return _path.empty(); }

			inline string fullpathname() const { return _path; }

			inline string pathname() const { auto pos = _path.find_last_of('/'); return pos != string::npos ? _path.substr(0,pos + 1) : ""; }

			inline string filename() const { auto pos = _path.find_last_of('/'); return pos != string::npos ? _path.substr(pos + 1) : _path; }

			inline string ext() const { auto pos = _path.find_last_of('.'); return pos != string::npos ? _path.substr(pos) : ""; }

			static inline string cwd () { char* _cwd = getcwd(nullptr,0); if (_cwd != nullptr) { string _path(_cwd + string("/")); free(_cwd); return _path; } return ""; }

			static inline string pwd() { string _path(program_invocation_name); return _path.substr(0, _path.find_last_of('/')); }

			static inline string realpath(const string&& pathname) { char* _rpath = ::realpath((char*)pathname.c_str(),nullptr); if (_rpath != nullptr) { string _path(_rpath); free(_rpath); return _path; } return pathname; }

			static inline string homepath() { struct passwd *pw = getpwuid(getuid()); return pw->pw_dir; }

			static inline string abspath(const string&& pathname, const string&& basepath = {}) {
				if (pathname.empty()) {
					return cwd();
				}
				string _path, _result("/");
				size_t start, end;
				list<string> parts,_abspath;
				bool is_directory = pathname.back() == '/';

				if (pathname == "~") {
					_path = homepath() + '/';
				}
				else if (strncmp(pathname.c_str(), "~/", 2) == 0) {
					_path = homepath() + '/' + pathname.substr(2);
				}
				else if (pathname[0] != '/') {
					_path = (basepath.empty() ? cwd() : basepath) + '/' + pathname;
				}
				else {
					_path = pathname;
				}

				for (start = 0, end = _path.find('/'); end != string::npos; start = end + 1, end = _path.find('/', start))
				{
					parts.emplace_back(string( _path.begin() + start, _path.begin() + end ));
				}

				parts.emplace_back(_path.begin() + start, _path.end());

				for (auto&& part : parts) {
					if (part.empty() || part == ".") {
						continue;
					}
					else if (part == "..") {
						if(!_abspath.empty()) _abspath.pop_back();
						continue;
					}
					_abspath.emplace_back(part);
				}

				for (; _abspath.size() > 1; _abspath.pop_front()) {
					_result += _abspath.front() + '/';
				}
				_result += _abspath.front() + (is_directory ? "/" : "");

				return _result;
			}
		};
	}
}